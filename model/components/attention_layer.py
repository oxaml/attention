# Implementation of Attention layer

import tensorflow as tf

# Attention Layer
class Attention(tf.layers.Layer):
  
  def __init__(self, d_model, h, attention_dropout, train):
    
    super(Attention, self).__init__()
    self.d_model = d_model # Dimensions for the embedding and intermediate output
    self.h = h # Number of heads
    self.attention_dropout = attention_dropout
    self.train = train
    
    # Linear Layers for projecting x to K,V, and y to Q
    # (For self-attention: projecting x to Q,K,V)
    self.Q = tf.layers.Dense(d_model, use_bias=False, name="Q")
    self.K = tf.layers.Dense(d_model, use_bias=False, name="K")
    self.V = tf.layers.Dense(d_model, use_bias=False, name="V")
    
    # Linear Layer for output
    self.output_linear = tf.layers.Dense(d_model, use_bias=False, name="output_linear")
    
    
  def split_heads(self, x):
    ''' Split input x to h heads 
      
      x = Q, K or V
      
      x: [batch, seq_len, d_model]
      
      split to ->
      
      x': [batch, h, seq_len, d_model/h]
      
      *During prediction, seq_len for decoder is the current length of 
        (the outputs + 1)
    '''
    
    with tf.name_scope("split_heads"):
      batch_size = tf.shape(x)[0]
      seq_len = tf.shape(x)[1]
      d_head = self.d_model // self.h # Integer division
      
      # Split the last dimension first, then transpose the h dimension forward
      x = tf.reshape(x, [batch_size, seq_len, self.h, d_head])
      x = tf.transpose(x, perm=[0, 2, 1, 3]) # To [batch, h, seq_len, d_head]
      
      return x
    
    
  def combine_heads(self, x):
    ''' MutiHead(Q,K,V) = Concat(head_1, ..., head_h) 
    
      x = [Attention_i(QW_i, KW_i, VW_i)] for all i in h
    
      x: [batch, h, seq_len, d_head]
      
      x': [batch, seq_len, h*d_head = d_model]
      
      *During prediction, seq_len for decoder is the current length of 
        (the outputs + 1)
    '''
    
    with tf.name_scope("combine_heads"):
      batch_size = tf.shape(x)[0]
      seq_len = tf.shape(x)[2]
      
      # transpose the h dimension backward, then reshape to concat on heads
      x = tf.transpose(x, perm=[0, 2, 1, 3]) # To [batch, seq_len, h, d_head]
      x = tf.reshape(x, [batch_size, seq_len, self.d_model])
      
      return x
   
  def call(self, x, y, mask, cache=None):
    ''' Apply Multi-Head attention to x, y
    
      x (Query): [batch, seq_len, d_model]
      y (Key, Value): [batch, seq_len, d_model]
      
      mask: attention bias and mask weights that will be added to the result of 
          the dot product before softmax.
          [batch, 1, 1, seq_len]
          for mask region, value = negative infinity, otherwise = 0
                                      
      cache (Used in prediction): caching the previous decoder self-attention K, V
          {"k": [batch, i, d_k],
           "v": [batch, i, d_v]}
          i is the current decoded length, in paper, d_k = d_v = d_model.
          
          During the autoregressive prediction, we need to keep a list of K,V used for 
          predicting the previous token, so that we can calculate the attention
          for this one with the previous ones.
    '''
    
    # Linear project x,y to q,k,v to learn how to build q,k,v for attention
    q = self.Q(x)
    k = self.K(y)
    v = self.V(y)
    
    # --- Using cache for prediction ---
    if cache is not None:
      # Concat previous ks,vs with new k,v at this position
      k = tf.concat([cache["k"], k], axis=1)
      v = tf.concat([cache["v"], v], axis=1)
      
      # Update cache
      cache["k"] = k
      cache["v"] = v
    # -------------------------------------
    
    # Split the heads for q,k,v
    q = self.split_heads(q)
    k = self.split_heads(k)
    v = self.split_heads(v)
    
    # Attention(q,k,v) = softmax((q*k^T)/sqrt(d_k))v
    
    d_k = self.d_model / self.h
    _q = q / tf.sqrt(d_k)
    
    attention_scores = tf.matmul(_q, k, transpose_b=True)
    attention_scores += mask # masking
    attention_weights = tf.nn.softmax(attention_scores)
    #tf.summary.histogram('attention_weights', attention_weights)
    
    # attention_dropout: not specified in paper, but appears in all other implementations
    if self.train:
      attention_weights = tf.nn.dropout(attention_weights, 
                                        1.0 - self.attention_dropout)
    
    attention_values = tf.matmul(attention_weights, v)
    
    # Concat heads and linearly project output
    attention_values = self.combine_heads(attention_values)
    attention_output = self.output_linear(attention_values)
    #writer = tf.summary.FileWriter("transformer_model/histogram_example")
    return attention_output
    
  
# Self Attention Layer
class SelfAttention(Attention):
  
  def call(self, x, mask, cache=None):
    return super().call(x, x, mask, cache)
