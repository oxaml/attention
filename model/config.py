"""
This contains data paramenters
"""
class Config:
  def __init__(self):
    self.de_en_data_sources = [
                          {
                              "url": "http://data.statmt.org/wmt17/translation-task/"
                                     "training-parallel-nc-v12.tgz",
                              "input": "news-commentary-v12.de-en.en",
                              "target": "news-commentary-v12.de-en.de",
                          },
                          {
                              "url": "http://www.statmt.org/wmt13/training-parallel-commoncrawl.tgz",
                              "input": "commoncrawl.de-en.en",
                              "target": "commoncrawl.de-en.de",
                          },
                          {
                              "url": "http://www.statmt.org/wmt13/training-parallel-europarl-v7.tgz",
                              "input": "europarl-v7.de-en.en",
                              "target": "europarl-v7.de-en.de",
                          }]
    self.de_en_evaluation_data = [
        
                          {
                              "url": "http://data.statmt.org/wmt17/translation-task/dev.tgz",
                              "input": "newstest2013.en",
                              "target": "newstest2013.de",
                          }]
    self.fr_en_data_sources = [
                          {
                              "url": "http://www.statmt.org/europarl/v7/fr-en.tgz",
                              "input": "europarl-v7.fr-en.en",
                              "target": "europarl-v7.fr-en.fr",
                          }]
    self.fr_en_evaluation_data = []
    
    """
    data directories
    """
    self.data_dir = "data/translate_ende"
    self.raw_dir = "data/translate_ende_raw"
    self.data_prefix = "europarl-v7"
    self.model_dir = "transformer_model"
    
    
    
    
    _TRAIN_DATA_SOURCES = [
    {
        "url": "http://data.statmt.org/wmt17/translation-task/"
               "training-parallel-nc-v12.tgz",
        "input": "news-commentary-v12.de-en.en",
        "target": "news-commentary-v12.de-en.de",
    },
    {
        "url": "http://www.statmt.org/wmt13/training-parallel-commoncrawl.tgz",
        "input": "commoncrawl.de-en.en",
        "target": "commoncrawl.de-en.de",
    },
    {
        "url": "http://www.statmt.org/wmt13/training-parallel-europarl-v7.tgz",
        "input": "europarl-v7.de-en.en",
        "target": "europarl-v7.de-en.de",
    },
]
# Use pre-defined minimum count to generate subtoken vocabulary.
_TRAIN_DATA_MIN_COUNT = 6

_EVAL_DATA_SOURCES = [
    {
        "url": "http://data.statmt.org/wmt17/translation-task/dev.tgz",
        "input": "newstest2013.en",
        "target": "newstest2013.de",
    }
]
    
